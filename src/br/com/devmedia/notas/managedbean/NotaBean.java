package br.com.devmedia.notas.managedbean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.devmedia.notas.model.Nota;
import br.com.devmedia.notas.rest.NotaREST;

@ManagedBean
@ViewScoped
public class NotaBean {

	private Integer id;
	private Nota nota;
	private List<Nota> notas;
	private NotaREST notaREST = new NotaREST();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Nota getNota() {
		return nota;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	public List<Nota> getNotas() {
		return notas;
	}

	@PostConstruct
	public void init() {
		notas = notaREST.listar();
	}

	public String exibir(Nota nota) {
		this.nota = nota;
		return "detalhes";
	}
	
	public void initDetalhes() {
		if (id != null) {
			this.nota = notaREST.getById(id);
		} else {
			this.nota = new Nota();
		}
		
	}
	
	public String editar() {
		if (this.id != null) {
			notaREST.update(this.nota);
		} else {
			notaREST.inserir(this.nota);
		}
		this.notas = notaREST.listar();
		return "index";
	}
	
	public String remover(Integer id) {
		notaREST.remove(id);
		this.notas = notaREST.listar();
		return "index";
	}

}
