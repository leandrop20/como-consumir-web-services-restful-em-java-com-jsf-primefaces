package br.com.devmedia.notas.rest;

import java.util.List;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import br.com.devmedia.notas.model.Nota;

public class NotaREST {

	private static final String API_URL =
		"http://devmedianotesapi.azurewebsites.net/api/";
	
	private Client client;
	private WebResource webResource;
	
	public NotaREST() {
		//show logs in console
//		client.addFilter(new LoggingFilter(System.out));
		ClientConfig clientConfig = new DefaultClientConfig(
			GensonProvider.class);
		client = Client.create(clientConfig);
		webResource = client.resource(API_URL);
	}
	
	public List<Nota> listar() {
		return webResource.path("Notes")
			.get(new GenericType<List<Nota>>() {});
	}
	
	public Nota getById(Integer id) {
		return webResource.path("Notes")
			.path(id.toString())
			.get(new GenericType<Nota>() {});
	}
	
	public void inserir(Nota nota) {
		webResource.path("Notes")
			.post(new GenericType<Nota>() {}, nota);
	}
	
	public void update(Nota nota) {
		webResource.path("Notes")
			.path(nota.getId().toString())
			.put(ClientResponse.class, nota);
	}
	
	public void remove(Integer id) {
		webResource.path("Notes")
			.path(id.toString())
			.delete();
	}
	
}